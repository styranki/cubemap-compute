
use exr::prelude::*;
use std::path::Path;

#[derive(Clone)]
pub struct Image {
    pub size: (u32, u32),
    pub data: Vec<[f32; 4]>,
}

pub fn read_exr(path: &Path) -> Image {
    let mut img = exr::prelude::read_first_rgba_layer_from_file(
        path,
        // instantiate your image type with the size of the image file
        |resolution, _| {
            let default_pixel = [0.0, 0.0, 0.0, 0.0];
            let empty_line =  vec![ default_pixel; resolution.width() ];
            let empty_image =  vec![ empty_line; resolution.height() ];
            empty_image
        },

        // transfer the colors from the file to your image type,
        // requesting all values to be converted to f32 numbers (you can also directly use f16 instead)
        // and you could also use `Sample` instead of `f32` to keep the original data type from the file
        // #FIXME rbg -> bgr because!?!
        |pixel_vector, position, (r,g,b,a): (f32, f32, f32, f32)| {
            pixel_vector[position.y()][position.x()] = [b, g, r, a]
        },
    ).unwrap();

    let img_size = img.layer_data.size;
    let img_data = img.layer_data.channel_data.pixels.drain(..).flatten().collect();


    Image {
        size: (img_size.0 as u32, img_size.1 as u32),
        data: img_data,
    }
}
