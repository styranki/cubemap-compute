use std::path::Path;
use renderer::cgmath as cgmath;
use wgpu::util::DeviceExt;
use exr_test::image;
use exr_test::bmp;
use exr_test::image::Image;
use exr_test::util::{Uniforms, Vertex, generate_cubemap_views, CUBE_VERTICES};
use renderer::texture::Texture;

pub const TEXTURE_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba32Float;
#[tokio::main]
async fn main() {
    env_logger::init();
    //let path = Path::new("./venice_sunset_4k.exr");
    let path = Path::new("./christmas_photo_studio_05_4k.exr");
    //let path = Path::new("./bell_park_pier_4k.exr");
    let img = image::read_exr(path);

    let instance = wgpu::Instance::new(wgpu::Backends::VULKAN);
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            compatible_surface: None,
            force_fallback_adapter: false,
        })
        .await
        .unwrap();

    dbg!(adapter.limits());
    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES,
                limits: wgpu::Limits::default(),
            },
            None,
        )
        .await
        .unwrap();

    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{
        label: None,
    });
    let output_size = (1024, 1024);
    let dst_tex = equirectangular_to_cubemap_texture(&device, &queue, img, output_size, &mut encoder);
    //let output_size = (64, 64);
    //let dst_tex = irradiance_convolution(&device, &queue, dst_tex, output_size, &mut encoder);
    bmp_debug_cubemap(&device, &queue, dst_tex, output_size, encoder).await;


}

fn equirectangular_to_cubemap_texture(device: &wgpu::Device,
                                      queue: &wgpu::Queue,
                                      img: Image,
                                      output_size: (u32, u32),
                                      encoder: &mut wgpu::CommandEncoder,
) -> Texture {

    let sample_count = 1;
    let texture_size = wgpu::Extent3d {
        width: img.size.0 as u32,
        height: img.size.1 as u32,
        depth_or_array_layers: 1,
    };

    let source_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Environment map"),
        size: texture_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::STORAGE_BINDING | wgpu::TextureUsages::COPY_DST,
    });

    queue.write_texture(
        wgpu::ImageCopyTexture {
            aspect: wgpu::TextureAspect::All,
            texture: &source_texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        },
        bytemuck::cast_slice(&img.data),
        wgpu::ImageDataLayout {
            offset: 0,
            bytes_per_row: std::num::NonZeroU32::new(16 * texture_size.width),
            rows_per_image: std::num::NonZeroU32::new(texture_size.height),
        },
        texture_size,
    );

    let source_view = source_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0,
        array_layer_count: None
    });

    let source_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        }
    );


    let dst_size = wgpu::Extent3d {
        width: output_size.0,
        height: output_size.1,
        depth_or_array_layers: 6,
    };

    let dst_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Compute dsp"),
        size: dst_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::COPY_DST |
        wgpu::TextureUsages::TEXTURE_BINDING |
        wgpu::TextureUsages::STORAGE_BINDING |
        wgpu::TextureUsages::COPY_SRC |
        wgpu::TextureUsages::RENDER_ATTACHMENT,
    });
/*
    let multisampled_texture_extent = wgpu::Extent3d {
            width: output_size.0,
            height: output_size.1,
            depth_or_array_layers: 1,
        };
        let multisampled_frame_descriptor = &wgpu::TextureDescriptor {
            size: multisampled_texture_extent,
            mip_level_count: 1,
            sample_count: sample_count,
            dimension: wgpu::TextureDimension::D2,
            format: TEXTURE_FORMAT,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            label: None,
        };
    let multisampled_view = device
            .create_texture(multisampled_frame_descriptor)
            .create_view(&wgpu::TextureViewDescriptor::default());

*/

    let dst_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            ..Default::default()
        }
    );    let vertex_buffer = device.create_buffer_init(
        &wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&CUBE_VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        }
    );

    let uniform_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        label: Some("Uniform buffer layout"),
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                count: None,
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                }
            }
        ]
    });



    let src_tex_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture{
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true }
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler (
                    wgpu::SamplerBindingType::Filtering,
                ),
                count: None,
            },
        ],
        label: Some("blur_bind_group_layout"),
    });
    let blur_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
        layout: &src_tex_bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry{
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&source_view),
            },
            wgpu::BindGroupEntry{
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&source_sampler),
            },
        ],
        label: Some("blur_bind_group"),

    });

    let conversion_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor{
        label: Some("Equirectangular to cubemap pipeline"),
        bind_group_layouts: &[
            &uniform_layout,
            &src_tex_bind_group_layout],
        push_constant_ranges: &[],
    });

    let vs_src = include_str!("../../shaders/cubemap.vert");
    let fs_src = include_str!("../../shaders/equirectangular_to_cubemap.frag");
    let vertex_shader = renderer::compile_shader(
        "vertex shader",
        vs_src,
        &[],
        shaderc::ShaderKind::Vertex
    );
    let fragment_shader = renderer::compile_shader(
        "fragment shader",
        fs_src,
        &[],
        shaderc::ShaderKind::Fragment
    );
    let vs_module = renderer::create_shader_module(&device, None, vertex_shader);
    let fs_module = renderer::create_shader_module(&device, None, fragment_shader);
    let vertex_descs = &[wgpu::VertexBufferLayout {
        array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x2],
    }];

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor{
        label: Some("Cubemap convert pipeline"),
        layout: Some(&conversion_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: vertex_descs,
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[wgpu::ColorTargetState {
                format: TEXTURE_FORMAT,
                blend: None,
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: None,
            polygon_mode: wgpu::PolygonMode::Fill,
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    });

    let (uniform_projection, uniform_views) = generate_cubemap_views();
    //let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{label: None});
    for i in 0..6 {
        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&[Uniforms {
                view: uniform_views[i],
                projection: uniform_projection,
            }]),
            usage: wgpu::BufferUsages::UNIFORM,
        });
        let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
            label: None,
            format: Some(TEXTURE_FORMAT),
            dimension: Some(wgpu::TextureViewDimension::D2Array),
            aspect:wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: i as u32,
            array_layer_count: Some(std::num::NonZeroU32::try_from(1).unwrap())
        });


        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
            layout: &uniform_layout,
            entries: &[
                wgpu::BindGroupEntry{
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(uniform_buffer.as_entire_buffer_binding()),
                },
            ],
            label: Some("uniform_bind_group"),

        });
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor{
            label: Some("Equirectangular to cubemap render pass"),
            color_attachments: &[
                wgpu::RenderPassColorAttachment{
                    view: &dst_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    }

                }
            ],
            depth_stencil_attachment: None,
        });
        rpass.set_pipeline(&render_pipeline);
        rpass.set_bind_group(0, &uniform_bind_group, &[]);
        rpass.set_bind_group(1, &blur_bind_group, &[]);
        rpass.set_vertex_buffer(0, vertex_buffer.slice(..));
        rpass.draw(0..36, 0..1);
    }

    let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::Cube),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0 as u32,
        array_layer_count: Some(std::num::NonZeroU32::try_from(6).unwrap())
    });
    //queue.submit(Some(encoder.finish()));

    Texture {
        texture: dst_texture,
        sampler: dst_sampler,
        view: dst_view,
    }

}
fn irradiance_convolution(device: &wgpu::Device,
                          queue: &wgpu::Queue,
                          src_tex: Texture,
                          output_size: (u32, u32),
                          encoder: &mut wgpu::CommandEncoder,
) -> Texture {

    let dst_size = wgpu::Extent3d {
        width: output_size.0,
        height: output_size.1,
        depth_or_array_layers: 6,
    };

    let dst_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Compute dsp"),
        size: dst_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::COPY_DST |
        wgpu::TextureUsages::TEXTURE_BINDING |
        wgpu::TextureUsages::STORAGE_BINDING |
        wgpu::TextureUsages::COPY_SRC |
        wgpu::TextureUsages::RENDER_ATTACHMENT,
    });



    let dst_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            ..Default::default()
        }
    );    let vertex_buffer = device.create_buffer_init(
        &wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&CUBE_VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        }
    );

    let uniform_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        label: Some("Uniform buffer layout"),
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                count: None,
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                }
            }
        ]
    });


    let src_tex_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture{
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::Cube,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true }
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler (
                    wgpu::SamplerBindingType::Filtering,
                ),
                count: None,
            },
        ],
        label: Some("Source tex bind layout"),
    });
    let blur_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
        layout: &src_tex_bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry{
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&src_tex.view),
            },
            wgpu::BindGroupEntry{
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&src_tex.sampler),
            },
        ],
        label: Some("Source tex bind group"),

    });

    let conversion_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor{
        label: Some("Irradiance convolution pipeline"),
        bind_group_layouts: &[
            &uniform_layout,
            &src_tex_bind_group_layout],
        push_constant_ranges: &[],
    });

    let vs_src = include_str!("../../shaders/cubemap.vert");
    let fs_src = include_str!("../../shaders/compute_irradiance.frag");
    let vertex_shader = renderer::compile_shader(
        "vertex shader",
        vs_src,
        &[],
        shaderc::ShaderKind::Vertex
    );
    let fragment_shader = renderer::compile_shader(
        "fragment shader",
        fs_src,
        &[],
        shaderc::ShaderKind::Fragment
    );
    let vs_module = renderer::create_shader_module(&device, None, vertex_shader);
    let fs_module = renderer::create_shader_module(&device, None, fragment_shader);
    let vertex_descs = &[wgpu::VertexBufferLayout {
        array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x2],
    }];

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor{
        label: Some("Compute irradiance pipeline"),
        layout: Some(&conversion_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: vertex_descs,
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[wgpu::ColorTargetState {
                format: TEXTURE_FORMAT,
                blend: None,
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: None,
            polygon_mode: wgpu::PolygonMode::Fill,
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    });

    let (uniform_projection, uniform_views) = generate_cubemap_views();
    //let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{label: None});
    for i in 0..6 {
        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&[Uniforms {
                view: uniform_views[i],
                projection: uniform_projection,
            }]),
            usage: wgpu::BufferUsages::UNIFORM,
        });
        let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
            label: None,
            format: Some(TEXTURE_FORMAT),
            dimension: Some(wgpu::TextureViewDimension::D2Array),
            aspect:wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: i as u32,
            array_layer_count: Some(std::num::NonZeroU32::try_from(1).unwrap())
        });


        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
            layout: &uniform_layout,
            entries: &[
                wgpu::BindGroupEntry{
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(uniform_buffer.as_entire_buffer_binding()),
                },
            ],
            label: Some("uniform_bind_group"),

        });
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor{
            label: Some("Irradiance computerender pass"),
            color_attachments: &[
                wgpu::RenderPassColorAttachment{
                    view: &dst_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    }

                }
            ],
            depth_stencil_attachment: None,
        });
        rpass.set_pipeline(&render_pipeline);
        rpass.set_bind_group(0, &uniform_bind_group, &[]);
        rpass.set_bind_group(1, &blur_bind_group, &[]);
        rpass.set_vertex_buffer(0, vertex_buffer.slice(..));
        rpass.draw(0..36, 0..1);
    }

    let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2Array),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0 as u32,
        array_layer_count: Some(std::num::NonZeroU32::try_from(6).unwrap())
    });
    //queue.submit(Some(encoder.finish()));

    Texture {
        texture: dst_texture,
        sampler: dst_sampler,
        view: dst_view,
    }

}

async fn bmp_debug_cubemap(device: &wgpu::Device,
                           queue: &wgpu::Queue,
                     tex: Texture,
                     size: (u32, u32),
                           mut encoder: wgpu::CommandEncoder,
) {
    //let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{label: None});
    let output_buffer = device.create_buffer(&wgpu::BufferDescriptor{
        label: None,
        // width * height * channels * bytes per channel * num faces
        size: (size.0 * size.1 * 4 * 4 * 6) as wgpu::BufferAddress,
        usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
        mapped_at_creation: false,
    });


    encoder.copy_texture_to_buffer(
        wgpu::ImageCopyTexture{
            texture: &tex.texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
            aspect: wgpu::TextureAspect::All,
        },
        wgpu::ImageCopyBuffer{
            buffer: &output_buffer,
            layout: wgpu::ImageDataLayout{
                offset: 0,
                bytes_per_row: std::num::NonZeroU32::new(16 * size.0),
                rows_per_image: std::num::NonZeroU32::new(size.1),
            }
        },
        wgpu::Extent3d {
            width: size.0,
            height: size.1,
            depth_or_array_layers: 6
        },
    );
    queue.submit(Some(encoder.finish()));
    let buffer_slice = output_buffer.slice(..);
    let buffer_future = buffer_slice.map_async(wgpu::MapMode::Read);

    device.poll(wgpu::Maintain::Wait);

    if let Ok(()) = buffer_future.await {
        let data = buffer_slice.get_mapped_range();
        let result: Vec<f32> = data
            .chunks_exact(4)
            .map(|b| f32::from_ne_bytes(b.try_into().unwrap()))
            .collect();
        for i in 0..6 {
            let pixels_per_face = (size.0 * size.1) as usize;
            let bmp_data: Vec<[u8; 3]> = result[(pixels_per_face*i)*4 as usize..(pixels_per_face*(i+1)*4)].chunks(4).map(|x| {
                let r = x[0];
                let g = x[1];
                let b = x[2];
                let a = x[3];

                [(r / 1.0 * 255.0) as u8,
                 (g / 1.0 * 255.0) as u8,
                 (b / 1.0 * 255.0) as u8]
            }).collect();
            bmp::write_image(bytemuck::cast_slice(&bmp_data), size.0, size.1, 8*3, &format!("file{}.bmp", i));

        }
    }

}
