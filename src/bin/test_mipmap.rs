use std::path::Path;
use exr_test::util;
use renderer::cgmath as cgmath;
use wgpu::util::DeviceExt;
use exr_test::image;
use exr_test::bmp;
use exr_test::image::Image;
use exr_test::util::{Uniforms, Vertex, generate_cubemap_views, CUBE_VERTICES};
use util::{TimestampQueries, PipelineStatisticsQueries, pipeline_statistics_offset};
use renderer::texture::Texture;

pub const TEXTURE_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba32Float;
#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    tracing::info!("Start");
    //let path = Path::new("./venice_sunset_4k.exr");
    let path = Path::new("./christmas_photo_studio_05_4k.exr");
    //let path = Path::new("./bell_park_pier_4k.exr");
    let img = image::read_exr(path);
    tracing::info!("Image loaded");

    let instance = wgpu::Instance::new(wgpu::Backends::all());
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            compatible_surface: None,
            force_fallback_adapter: false,
        })
        .await
        .unwrap();

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                    | wgpu::Features::WRITE_TIMESTAMP_INSIDE_PASSES
                    | wgpu::Features::TIMESTAMP_QUERY
                    | wgpu::Features::PIPELINE_STATISTICS_QUERY,
                limits: wgpu::Limits::default(),
            },
            None,
        )
        .await
        .unwrap();

    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{
        label: None,
    });
    let output_size = (1024, 1024);
    tracing::info!("WGPU init finished");
    let src_tex = util::equirectangular_to_cubemap_texture(&device, &queue, img, output_size, &mut encoder);
    let texture_format = TEXTURE_FORMAT;
    let query_sets = generate_querysets(&device, &queue);
    util::generate_mipmaps(&mut encoder,
                           &device,
                           &src_tex.texture,
                           &query_sets,
                           1,
                           texture_format);


    queue.submit(Some(encoder.finish()));

    tracing::info!("Mipmaps generated");
/*
    for mip_lvl in (0..7) {
        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{
            label: None,
        });
        util::bmp_debug_texture(&device, &queue, &src_tex, output_size, mip_lvl, encoder);
    }
    */

    if let Some(ref query_sets) = query_sets {
        // We can ignore the future as we're about to wait for the device.
        let juttu = query_sets
            .data_buffer
            .slice(..)
            .map_async(wgpu::MapMode::Read, |res| {
                dbg!(res);
            });
        dbg!(juttu);
        // Wait for device to be done rendering mipmaps
        tracing::warn!("Polling");
        device.poll(wgpu::Maintain::Wait);
        tracing::warn!("Polled");
        // This is guaranteed to be ready.
        tracing::info!("{}", query_sets.data_buffer.size());
        tracing::info!("{:?}", query_sets.data_buffer.usage());
        let timestamp_view = query_sets
            .data_buffer
            .slice(..std::mem::size_of::<TimestampQueries>() as wgpu::BufferAddress)
            .get_mapped_range();
        let pipeline_stats_view = query_sets
            .data_buffer
            .slice(pipeline_statistics_offset()..)
            .get_mapped_range();
        // Convert the raw data into a useful structure
        let timestamp_data: &TimestampQueries = bytemuck::from_bytes(&*timestamp_view);
        let pipeline_stats_data: &PipelineStatisticsQueries =
            bytemuck::from_bytes(&*pipeline_stats_view);
        // Iterate over the data
        for (idx, (timestamp, pipeline)) in timestamp_data
            .iter()
            .zip(pipeline_stats_data.iter())
            .enumerate()
        {
            // Figure out the timestamp differences and multiply by the period to get nanoseconds
            let nanoseconds =
                (timestamp.end - timestamp.start) as f32 * query_sets.timestamp_period;
            // Nanoseconds is a bit small, so lets use microseconds.
            let microseconds = nanoseconds / 1000.0;
            // Print the data!
            println!(
                "Generating mip level {} took {:.3} μs and called the fragment shader {} times",
                idx + 1,
                microseconds,
                pipeline
            );
        }
    }

}

fn generate_querysets(device: &wgpu::Device,
                      queue: &wgpu::Queue,
) -> Option<util::QuerySets> {
        // If both kinds of query are supported, use queries
        let query_sets = if device
            .features()
            .contains(wgpu::Features::TIMESTAMP_QUERY | wgpu::Features::PIPELINE_STATISTICS_QUERY)
        {
            tracing::info!("Generating query sets");
            // For N total mips, it takes N - 1 passes to generate them, and we're measuring those.
            let mip_passes = util::MIP_LEVEL_COUNT - 1;

            // Create the timestamp query set. We need twice as many queries as we have passes,
            // as we need a query at the beginning and at the end of the operation.
            let timestamp = device.create_query_set(&wgpu::QuerySetDescriptor {
                label: Some("mipmap pass timestamps"),
                count: mip_passes * 2,
                ty: wgpu::QueryType::Timestamp,
            });
            // Timestamp queries use an device-specific timestamp unit. We need to figure out how many
            // nanoseconds go by for the timestamp to be incremented by one. The period is this value.
            let timestamp_period = queue.get_timestamp_period();

            // We only need one pipeline statistics query per pass.
            let pipeline_statistics = device.create_query_set(&wgpu::QuerySetDescriptor {
                label: Some("mipmap pass shader invocations"),
                count: mip_passes,
                ty: wgpu::QueryType::PipelineStatistics(
                    wgpu::PipelineStatisticsTypes::FRAGMENT_SHADER_INVOCATIONS,
                ),
            });

            // This databuffer has to store all of the query results, 2 * passes timestamp queries
            // and 1 * passes statistics queries. Each query returns a u64 value.
            let data_buffer = device.create_buffer(&wgpu::BufferDescriptor {
                label: Some("query buffer"),
                size: pipeline_statistics_offset()
                    + std::mem::size_of::<PipelineStatisticsQueries>() as wgpu::BufferAddress,
                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::MAP_READ,
                mapped_at_creation: false,
            });

            Some(util::QuerySets {
                timestamp,
                timestamp_period,
                pipeline_statistics,
                data_buffer,
            })
        } else {
            tracing::info!("Missing needed features for query sets");
            None
        };

    query_sets
}
