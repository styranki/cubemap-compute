use std::fs::File;
use std::io::Write;
use byteorder::{LittleEndian, WriteBytesExt};
const FILE_HEADER_SIZE: u32 = 14;
const INFO_HEADER_SIZE: u32 = 40;

pub fn write_image(image: &[u8], width: u32, height: u32, bits_per_pixel: u16, filename: &str) {
    let width_in_bytes = width * bits_per_pixel as u32;
    let padding = [0u8; 4];
    let padding_size= ((4 - (width_in_bytes) % 4) % 4) as usize;
    let stride = width_in_bytes + padding_size as u32;
    let file_header = create_bitmap_header(height, stride);
    let info_header = create_bitmap_info_header(height, width, bits_per_pixel);

    let mut file = File::create(filename).expect("Failed to create file");
    file.write(&file_header).unwrap();
    file.write(&info_header).unwrap();

    image.chunks(width_in_bytes as usize).for_each(|line| {
        file.write(line).unwrap();
        file.write(&padding[0..padding_size]).unwrap();
    })
}

fn create_bitmap_header(height: u32, stride: u32) -> Vec<u8> {
    let filesize = FILE_HEADER_SIZE + INFO_HEADER_SIZE + (stride * height);
    let mut file_header = Vec::new();
    /*
    let mut file_header: &[u8] = [
    0, 0,        // signature
    0, 0, 0, 0,  // image file size in bytes
    0, 0, 0, 0,  // reserved
    0, 0, 0, 0,  // start of pixel array
];
     */

    // Char B
    file_header.write_u8(66);
    // Char B
    file_header.write_u8(77);
    file_header.write_u32::<LittleEndian>(filesize);
    file_header.write_u32::<LittleEndian>(0);
    file_header.write_u32::<LittleEndian>(FILE_HEADER_SIZE + INFO_HEADER_SIZE);
    file_header
}

fn create_bitmap_info_header(height: u32, width: u32, bits_per_pixel: u16) -> Vec<u8> {

    /*
    let mut info_header: &[u8] = [
    0,0,0,0, /// header size
    0,0,0,0, /// image width
    0,0,0,0, /// image height
    0,0,     /// number of color planes
    0,0,     /// bits per pixel
    0,0,0,0, /// compression
    0,0,0,0, /// image size
    0,0,0,0, /// horizontal resolution
    0,0,0,0, /// vertical resolution
    0,0,0,0, /// colors in color table
    0,0,0,0, /// important color count
]
     */
    let mut info_header = Vec::new();
    info_header.write_u32::<LittleEndian>(INFO_HEADER_SIZE);
    info_header.write_u32::<LittleEndian>(width);
    info_header.write_u32::<LittleEndian>(height);
    info_header.write_u16::<LittleEndian>(1);
    info_header.write_u16::<LittleEndian>(bits_per_pixel);
    let rest = vec![0u8; 6*4];
    info_header.write(&rest);

    info_header
}
