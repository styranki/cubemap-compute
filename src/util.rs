use renderer::cgmath as cgmath;
use renderer::wgpu as wgpu;
use cgmath::Matrix4;
use cgmath::EuclideanSpace;
use crate::image::Image;
use crate::bmp;
use wgpu::util::DeviceExt;
use renderer::texture::Texture;
use bytemuck::{Pod, Zeroable};


pub const TEXTURE_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba32Float;
pub const MIP_LEVEL_COUNT: u32 = 9;
pub const MIP_PASS_COUNT: u32 = MIP_LEVEL_COUNT - 1;

#[repr(C)]
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Vertex {
    position: [f32; 3],
    normal: [f32; 3],
    uv: [f32; 2],
}
unsafe impl bytemuck::Zeroable for Vertex {}
unsafe impl bytemuck::Pod for Vertex {}


#[derive(Copy, Clone)]
pub struct Uniforms {
    pub projection: cgmath::Matrix4<f32>,
    pub view: cgmath::Matrix4<f32>
}
unsafe impl bytemuck::Zeroable for Uniforms {}
unsafe impl bytemuck::Pod for Uniforms {}


#[repr(C)]
#[derive(Clone, Copy)]
pub struct TimestampData {
    pub start: u64,
    pub end: u64,
}
unsafe impl bytemuck::Zeroable for TimestampData {}
unsafe impl bytemuck::Pod for TimestampData {}

pub type TimestampQueries = [TimestampData; MIP_PASS_COUNT as usize];
pub type PipelineStatisticsQueries = [u64; MIP_PASS_COUNT as usize];

pub fn pipeline_statistics_offset() -> wgpu::BufferAddress {
    (std::mem::size_of::<TimestampQueries>() as wgpu::BufferAddress)
        .max(wgpu::QUERY_RESOLVE_BUFFER_ALIGNMENT)
}

pub struct QuerySets {
    pub timestamp: wgpu::QuerySet,
    pub timestamp_period: f32,
    pub pipeline_statistics: wgpu::QuerySet,
    pub data_buffer: wgpu::Buffer,
}


pub fn generate_mipmaps(
    encoder: &mut wgpu::CommandEncoder,
    device: &wgpu::Device,
    texture: &wgpu::Texture,
    query_sets: &Option<QuerySets>,
    mip_count: u32,
    texture_format: wgpu::TextureFormat,
) {
    let vs_src = include_str!("../shaders/blit.vert");
    let fs_src = include_str!("../shaders/blit.frag");
    let vertex_shader = renderer::compile_shader(
        "vertex shader",
        vs_src,
        &[],
        shaderc::ShaderKind::Vertex
    );
    let fragment_shader = renderer::compile_shader(
        "fragment shader",
        fs_src,
        &[],
        shaderc::ShaderKind::Fragment
    );
    let vs_module = renderer::create_shader_module(&device, None, vertex_shader);
    let fs_module = renderer::create_shader_module(&device, None, fragment_shader);

    let vertex_buffer_layout = wgpu::VertexBufferLayout {
        array_stride: 8 as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes:&wgpu::vertex_attr_array![0 => Float32x2],
    };
    let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor{
        label: Some("blit"),
        layout: None,
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: &[vertex_buffer_layout],
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[Some(texture_format.into())],
        }),
        primitive: wgpu::PrimitiveState{
            topology: wgpu::PrimitiveTopology::TriangleStrip,
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    });
    let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Quad vertices"),
        contents: bytemuck::cast_slice(&[
            0.0_f32, 1.0,
            0.0, 0.0,
            1.0, 1.0,
            1.0, 0.0
        ]),
        usage: wgpu::BufferUsages::VERTEX,
    });

    let bind_group_layout = pipeline.get_bind_group_layout(0);
    let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
        label: Some("mip"),
        address_mode_u: wgpu::AddressMode::ClampToEdge,
        address_mode_v: wgpu::AddressMode::ClampToEdge,
        address_mode_w: wgpu::AddressMode::ClampToEdge,
        mag_filter: wgpu::FilterMode::Linear,
        min_filter: wgpu::FilterMode::Nearest,
        mipmap_filter: wgpu::FilterMode::Nearest,
        ..Default::default()
    });

    let views = (0..mip_count)
        .map(|mip| {
            texture.create_view(&wgpu::TextureViewDescriptor{
                label: Some("mip"),
                format: None,
                dimension: Some(wgpu::TextureViewDimension::D2),
                aspect: wgpu::TextureAspect::All,
                base_mip_level: mip,
                mip_level_count: std::num::NonZeroU32::new(1),
                base_array_layer: 0,
                array_layer_count: std::num::NonZeroU32::new(1),
            })
        }).collect::<Vec<_>>();

    for target_mip in 1..mip_count as usize {
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&views[target_mip - 1]),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                },
            ],
            label: None,
        });

        let pipeline_query_index_base = target_mip as u32 - 1;
        let timestamp_query_index_base = (target_mip as u32 - 1) * 2;

        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &views[target_mip],
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLUE),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });
        if let Some(ref query_sets) = query_sets {
            rpass.write_timestamp(&query_sets.timestamp, timestamp_query_index_base);
            /*rpass.begin_pipeline_statistics_query(
                &query_sets.pipeline_statistics,
                pipeline_query_index_base,
            );*/
        }
        rpass.set_pipeline(&pipeline);
        rpass.set_bind_group(0, &bind_group, &[]);
        rpass.set_vertex_buffer(0, vertex_buffer.slice(..));
        rpass.draw(0..4, 0..1);
        if let Some(ref query_sets) = query_sets {
            //rpass.end_pipeline_statistics_query();
            rpass.write_timestamp(&query_sets.timestamp, timestamp_query_index_base + 1);
        }
    }


    if let Some(ref query_sets) = query_sets {
        let timestamp_query_count = MIP_PASS_COUNT * 2;
        encoder.resolve_query_set(
            &query_sets.timestamp,
            0..timestamp_query_count,
            &query_sets.data_buffer,
            0,
        );
        encoder.resolve_query_set(
            &query_sets.pipeline_statistics,
            0..MIP_PASS_COUNT,
            &query_sets.data_buffer,
            pipeline_statistics_offset(),
        );
    }


}
pub fn equirectangular_to_cubemap_texture(device: &wgpu::Device,
                                      queue: &wgpu::Queue,
                                      img: Image,
                                      output_size: (u32, u32),
                                      encoder: &mut wgpu::CommandEncoder,
) -> Texture {

    let texture_size = wgpu::Extent3d {
        width: img.size.0 as u32,
        height: img.size.1 as u32,
        depth_or_array_layers: 1,
    };

    let source_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Environment map"),
        size: texture_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
    });

    queue.write_texture(
        wgpu::ImageCopyTexture {
            aspect: wgpu::TextureAspect::All,
            texture: &source_texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        },
        bytemuck::cast_slice(&img.data),
        wgpu::ImageDataLayout {
            offset: 0,
            bytes_per_row: std::num::NonZeroU32::new(16 * texture_size.width),
            rows_per_image: std::num::NonZeroU32::new(texture_size.height),
        },
        texture_size,
    );

    let source_view = source_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: std::num::NonZeroU32::new(1),
        base_array_layer: 0,
        array_layer_count: std::num::NonZeroU32::new(1),
    });

    let source_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        }
    );


    let dst_size = wgpu::Extent3d {
        width: output_size.0,
        height: output_size.1,
        depth_or_array_layers: 6,
    };

    let dst_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Compute dsp"),
        size: dst_size,
        mip_level_count: MIP_LEVEL_COUNT,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::COPY_DST |
        wgpu::TextureUsages::COPY_SRC |
        wgpu::TextureUsages::TEXTURE_BINDING |
        wgpu::TextureUsages::RENDER_ATTACHMENT,
    });

    let dst_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            ..Default::default()
        }
    );    let vertex_buffer = device.create_buffer_init(
        &wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&CUBE_VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        }
    );

    let uniform_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        label: Some("Uniform buffer layout"),
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                count: None,
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                }
            }
        ]
    });



    let src_tex_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture{
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true }
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler (
                    wgpu::SamplerBindingType::Filtering,
                ),
                count: None,
            },
        ],
        label: Some("blur_bind_group_layout"),
    });
    let blur_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
        layout: &src_tex_bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry{
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&source_view),
            },
            wgpu::BindGroupEntry{
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&source_sampler),
            },
        ],
        label: Some("blur_bind_group"),

    });

    let conversion_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor{
        label: Some("Equirectangular to cubemap pipeline"),
        bind_group_layouts: &[
            &uniform_layout,
            &src_tex_bind_group_layout],
        push_constant_ranges: &[],
    });

    let vs_src = include_str!("../shaders/cubemap.vert");
    let fs_src = include_str!("../shaders/equirectangular_to_cubemap.frag");
    let vertex_shader = renderer::compile_shader(
        "vertex shader",
        vs_src,
        &[],
        shaderc::ShaderKind::Vertex
    );
    let fragment_shader = renderer::compile_shader(
        "fragment shader",
        fs_src,
        &[],
        shaderc::ShaderKind::Fragment
    );
    let vs_module = renderer::create_shader_module(&device, None, vertex_shader);
    let fs_module = renderer::create_shader_module(&device, None, fragment_shader);
    let vertex_descs = &[wgpu::VertexBufferLayout {
        array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x2],
    }];

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor{
        label: Some("Cubemap convert pipeline"),
        layout: Some(&conversion_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &vs_module,
            entry_point: "main",
            buffers: vertex_descs,
        },
        fragment: Some(wgpu::FragmentState {
            module: &fs_module,
            entry_point: "main",
            targets: &[Some(wgpu::ColorTargetState {
                format: TEXTURE_FORMAT,
                blend: None,
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: None,
            polygon_mode: wgpu::PolygonMode::Fill,
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    });

    let (uniform_projection, uniform_views) = generate_cubemap_views();
    //let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{label: None});
    for i in 0..6 {
        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor{
            label: Some("Cube vertices"),
            contents: bytemuck::cast_slice(&[Uniforms {
                view: uniform_views[i],
                projection: uniform_projection,
            }]),
            usage: wgpu::BufferUsages::UNIFORM,
        });
        let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
            label: None,
            format: Some(TEXTURE_FORMAT),
            dimension: Some(wgpu::TextureViewDimension::D2Array),
            aspect:wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: std::num::NonZeroU32::new(1),
            base_array_layer: i as u32,
            array_layer_count: Some(std::num::NonZeroU32::try_from(1).unwrap())
        });


        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
            layout: &uniform_layout,
            entries: &[
                wgpu::BindGroupEntry{
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(uniform_buffer.as_entire_buffer_binding()),
                },
            ],
            label: Some("uniform_bind_group"),

        });
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor{
            label: Some("Equirectangular to cubemap render pass"),
            color_attachments: &[
                Some(wgpu::RenderPassColorAttachment{
                    view: &dst_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    }

                })
            ],
            depth_stencil_attachment: None,
        });
        rpass.set_pipeline(&render_pipeline);
        rpass.set_bind_group(0, &uniform_bind_group, &[]);
        rpass.set_bind_group(1, &blur_bind_group, &[]);
        rpass.set_vertex_buffer(0, vertex_buffer.slice(..));
        rpass.draw(0..36, 0..1);
    }

    let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0 as u32,
        array_layer_count: Some(std::num::NonZeroU32::try_from(1).unwrap())
    });
    //queue.submit(Some(encoder.finish()));

    Texture {
        texture: dst_texture,
        sampler: dst_sampler,
        view: dst_view,
    }

}
pub fn generate_cubemap_views() -> (Matrix4<f32>, [Matrix4<f32>; 6]) {
let zero = cgmath::Point3::origin();
    let unit_x = cgmath::Vector3::unit_x();
    let unit_y = cgmath::Vector3::unit_y();
    let unit_z = cgmath::Vector3::unit_z();

    let uniform_projection = cgmath::perspective(cgmath::Deg(90.0), 1.0, 0.1, 10.0);
    let uniform_views: [Matrix4<f32>; 6] = [
        cgmath::Matrix4::look_at_rh(zero, zero + -1.0 * unit_x, unit_y),
        cgmath::Matrix4::look_at_rh(zero, zero + unit_x, unit_y),
        cgmath::Matrix4::look_at_rh(zero, zero + -1.0 * unit_y, unit_x),
        cgmath::Matrix4::look_at_rh(zero, zero + unit_y, -1.0 * unit_x),
        cgmath::Matrix4::look_at_rh(zero, zero + -1.0 * unit_z, unit_y),
        cgmath::Matrix4::look_at_rh(zero, zero +  unit_z, unit_y),
    ];

    (uniform_projection, uniform_views)
}

pub const CUBE_VERTICES: &[f32] = &[
            // back face
            -1.0, -1.0, -1.0,  0.0,  0.0, -1.0, 0.0, 0.0, // bottom-left
             1.0,  1.0, -1.0,  0.0,  0.0, -1.0, 1.0, 1.0, // top-right
             1.0, -1.0, -1.0,  0.0,  0.0, -1.0, 1.0, 0.0, // bottom-right
             1.0,  1.0, -1.0,  0.0,  0.0, -1.0, 1.0, 1.0, // top-right
            -1.0, -1.0, -1.0,  0.0,  0.0, -1.0, 0.0, 0.0, // bottom-left
            -1.0,  1.0, -1.0,  0.0,  0.0, -1.0, 0.0, 1.0, // top-left
            // ront face
            -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 0.0, 0.0, // bottom-left
             1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 1.0, 0.0, // bottom-right
             1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0, 1.0, // top-right
             1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0, 1.0, // top-right
            -1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 0.0, 1.0, // top-left
            -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 0.0, 0.0, // bottom-left
            // left face
            -1.0,  1.0,  1.0, -1.0,  0.0,  0.0, 1.0, 0.0, // top-right
            -1.0,  1.0, -1.0, -1.0,  0.0,  0.0, 1.0, 1.0, // top-left
            -1.0, -1.0, -1.0, -1.0,  0.0,  0.0, 0.0, 1.0, // bottom-left
            -1.0, -1.0, -1.0, -1.0,  0.0,  0.0, 0.0, 1.0, // bottom-left
            -1.0, -1.0,  1.0, -1.0,  0.0,  0.0, 0.0, 0.0, // bottom-right
            -1.0,  1.0,  1.0, -1.0,  0.0,  0.0, 1.0, 0.0, // top-right
            // right face
             1.0,  1.0,  1.0,  1.0,  0.0,  0.0, 1.0, 0.0, // top-left
             1.0, -1.0, -1.0,  1.0,  0.0,  0.0, 0.0, 1.0, // bottom-right
             1.0,  1.0, -1.0,  1.0,  0.0,  0.0, 1.0, 1.0, // top-right
             1.0, -1.0, -1.0,  1.0,  0.0,  0.0, 0.0, 1.0, // bottom-right
             1.0,  1.0,  1.0,  1.0,  0.0,  0.0, 1.0, 0.0, // top-left
             1.0, -1.0,  1.0,  1.0,  0.0,  0.0, 0.0, 0.0, // bottom-left
            // bottom face
            -1.0, -1.0, -1.0,  0.0, -1.0,  0.0, 0.0, 1.0, // top-right
             1.0, -1.0, -1.0,  0.0, -1.0,  0.0, 1.0, 1.0, // top-left
             1.0, -1.0,  1.0,  0.0, -1.0,  0.0, 1.0, 0.0, // bottom-left
             1.0, -1.0,  1.0,  0.0, -1.0,  0.0, 1.0, 0.0, // bottom-left
            -1.0, -1.0,  1.0,  0.0, -1.0,  0.0, 0.0, 0.0, // bottom-right
            -1.0, -1.0, -1.0,  0.0, -1.0,  0.0, 0.0, 1.0, // top-right
            // top face
            -1.0,  1.0, -1.0,  0.0,  1.0,  0.0, 0.0, 1.0, // top-left
             1.0,  1.0 , 1.0,  0.0,  1.0,  0.0, 1.0, 0.0, // bottom-right
             1.0,  1.0, -1.0,  0.0,  1.0,  0.0, 1.0, 1.0, // top-right
             1.0,  1.0,  1.0,  0.0,  1.0,  0.0, 1.0, 0.0, // bottom-right
            -1.0,  1.0, -1.0,  0.0,  1.0,  0.0, 0.0, 1.0, // top-left
            -1.0,  1.0,  1.0,  0.0,  1.0,  0.0, 0.0, 0.0  // bottom-left
        ];

#[tracing::instrument(skip_all)]
pub fn bmp_debug_texture(device: &wgpu::Device,
                           queue: &wgpu::Queue,
                           tex: &Texture,
                           size: (u32, u32),
                           mip_level: u32,
                           mut encoder: wgpu::CommandEncoder,
) {
    tracing::info!("Start");
    let mip_mult = 2_u32.pow(mip_level);
    let mip_size = (size.0/mip_mult, size.1/mip_mult);

    let output_buffer = device.create_buffer(&wgpu::BufferDescriptor{
        label: None,
        // width * height * channels * bytes per channel * num faces
        size: (mip_size.0 * mip_size.1 * 4 * 4 * 6) as wgpu::BufferAddress,
        usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
        mapped_at_creation: false,
    });


    encoder.copy_texture_to_buffer(
        wgpu::ImageCopyTexture{
            texture: &tex.texture,
            mip_level,
            origin: wgpu::Origin3d::ZERO,
            aspect: wgpu::TextureAspect::All,
        },
        wgpu::ImageCopyBuffer{
            buffer: &output_buffer,
            layout: wgpu::ImageDataLayout{
                offset: 0,
                bytes_per_row: std::num::NonZeroU32::new(16 * mip_size.0),
                rows_per_image: std::num::NonZeroU32::new(mip_size.1),
            }
        },
        wgpu::Extent3d {
            width: mip_size.0,
            height: mip_size.1,
            depth_or_array_layers: 6
        },
    );
    tracing::info!("Submit");
    queue.submit(Some(encoder.finish()));
    tracing::info!("Texture copied to tex");
    let buffer_slice = output_buffer.slice(..);
    let buffer_future = buffer_slice.map_async(wgpu::MapMode::Read, |buf| {
        dbg!(buf);
    });
    device.poll(wgpu::Maintain::Wait);
    tracing::info!("Texture copied to buffer");
        let data = buffer_slice.get_mapped_range();
        let result: Vec<f32> = data
            .chunks_exact(4)
            .map(|b| f32::from_ne_bytes(b.try_into().unwrap()))
            .collect();
        let pixels_per_face = (mip_size.0 * mip_size.1) as usize;
        for face_idx in  0..6 {
        let bmp_data: Vec<[u8; 3]> = result[face_idx * 4 *  pixels_per_face..4*(face_idx + 1)* pixels_per_face].chunks(4).map(|x| {
            let r = x[0];
            let g = x[1];
            let b = x[2];
            let a = x[3];

            [(r / 1.0 * 255.0) as u8,
             (g / 1.0 * 255.0) as u8,
             (b / 1.0 * 255.0) as u8]
        }).collect();

        tracing::info!("Bmp image produced");
        bmp::write_image(bytemuck::cast_slice(&bmp_data), mip_size.0, mip_size.1, 8*3, &format!("{}x{}_{}.bmp", mip_size.0, mip_size.1, face_idx));
        tracing::info!("Bmp image saved");
    }
}
