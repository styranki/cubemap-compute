use std::path::Path;

mod image;
mod bmp;

#[tokio::main]
async fn main() {
    let path = Path::new("./venice_sunset_4k.exr");
    //let path = Path::new("./christmas_photo_studio_05_4k.exr");
    let img = image::read_exr(path);

    let instance = wgpu::Instance::new(wgpu::Backends::all());
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            compatible_surface: None,
            force_fallback_adapter: false,
        })
        .await
        .unwrap();

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES,
                limits: wgpu::Limits::default(),
            },
            None,
        )
        .await
        .unwrap();

    pub const TEXTURE_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba32Float;
    let texture_size = wgpu::Extent3d {
        width: img.size.0 as u32,
        height: img.size.1 as u32,
        depth_or_array_layers: 1,
    };

    let source_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Environment map"),
        size: texture_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::STORAGE_BINDING | wgpu::TextureUsages::COPY_DST,
    });

    queue.write_texture(
        wgpu::ImageCopyTexture {
            aspect: wgpu::TextureAspect::All,
            texture: &source_texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        },
        bytemuck::cast_slice(&img.data),
        wgpu::ImageDataLayout {
            offset: 0,
            bytes_per_row: std::num::NonZeroU32::new(16 * texture_size.width),
            rows_per_image: std::num::NonZeroU32::new(texture_size.height),
        },
        texture_size,
    );

    let source_view = source_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0,
        array_layer_count: None
    });

    let source_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::Repeat,
            address_mode_v: wgpu::AddressMode::Repeat,
            address_mode_w: wgpu::AddressMode::Repeat,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        }
    );
    let dst_size = wgpu::Extent3d {
        width: 256,
        height: 256,
        depth_or_array_layers: 6,
    };
    let dst_texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Compute dsp"),
        size: dst_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: TEXTURE_FORMAT,
        usage: wgpu::TextureUsages::COPY_DST |
        wgpu::TextureUsages::TEXTURE_BINDING |
        wgpu::TextureUsages::STORAGE_BINDING |
        wgpu::TextureUsages::COPY_SRC,
    });
    /*
    let data: Vec<f32> =std::iter::repeat(0.0f32).take(256*256*4*6).collect();
    queue.write_texture(
        wgpu::ImageCopyTexture {
            texture: &dst_texture,
            aspect: wgpu::TextureAspect::All,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
        },
        &bytemuck::cast_slice(&data),
        wgpu::ImageDataLayout{
            offset: 0,
            bytes_per_row: Some(std::num::NonZeroU32::try_from(dst_size.width * 16).unwrap()),
            rows_per_image: Some(std::num::NonZeroU32::try_from(dst_size.height).unwrap())
        },
        dst_size,
    );
    */
    let dst_view = dst_texture.create_view(&wgpu::TextureViewDescriptor{
        label: None,
        format: Some(TEXTURE_FORMAT),
        dimension: Some(wgpu::TextureViewDimension::D2Array),
        aspect:wgpu::TextureAspect::All,
        base_mip_level: 0,
        mip_level_count: None,
        base_array_layer: 0,
        array_layer_count: Some(std::num::NonZeroU32::try_from(6).unwrap())
    });

    let dst_sampler = device.create_sampler(
        &wgpu::SamplerDescriptor{
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        }
    );


    let blur_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor{
        entries: &[
            wgpu::BindGroupLayoutEntry{
                binding: 0,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Texture{
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true }
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 1,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Sampler{
                    comparison: false,
                    filtering: true,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 2,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::StorageTexture{
                    access: wgpu::StorageTextureAccess::WriteOnly,
                    format: TEXTURE_FORMAT,
                    view_dimension: wgpu::TextureViewDimension::D2Array,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry{
                binding: 3,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Sampler{
                    comparison: false,
                    filtering: true,
                },
                count: None,
            }
        ],
        label: Some("blur_bind_group_layout"),
    });

    let blur_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor{
        label: Some("blur"),
        bind_group_layouts: &[&blur_bind_group_layout],
        push_constant_ranges: &[],
    });

    let blur_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor{
        layout: &blur_bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry{
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&source_view),
            },
            wgpu::BindGroupEntry{
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&source_sampler),
            },
            wgpu::BindGroupEntry{
                binding: 2,
                resource: wgpu::BindingResource::TextureView(&dst_view),
            },
            wgpu::BindGroupEntry{
                binding: 3,
                resource: wgpu::BindingResource::Sampler(&dst_sampler),
            },
        ],
        label: Some("blur_bind_group"),

    });
    let compute_shader = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(include_str!("../compute.wgsl"))),
    });
    let blur_pipeline = device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor{
        label: Some("Texture blur pipeline"),
        layout: Some(&blur_pipeline_layout),
        module: &compute_shader,
        entry_point: "main",
    });
    let output_buffer = device.create_buffer(&wgpu::BufferDescriptor{
        label: None,
        size: (dst_size.width * dst_size.height * 4 * 4 * 6) as wgpu::BufferAddress,
        usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
        mapped_at_creation: false,
    });

    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor{label: None});
    {
        let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor{
            label: None,
        });
        cpass.set_pipeline(&blur_pipeline);
        cpass.set_bind_group(0, &blur_bind_group, &[]);
        cpass.insert_debug_marker("Compute dst texture");
        cpass.dispatch(256,256,6);
    }

    encoder.copy_texture_to_buffer(
        wgpu::ImageCopyTexture{
            texture: &dst_texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
            aspect: wgpu::TextureAspect::All,
        },
        wgpu::ImageCopyBuffer{
            buffer: &output_buffer,
            layout: wgpu::ImageDataLayout{
                offset: 0,
                bytes_per_row: std::num::NonZeroU32::new(16 * dst_size.width),
                rows_per_image: std::num::NonZeroU32::new(dst_size.height),
            }
        },
        dst_size,
    );
    let t1 = std::time::Instant::now();
    dbg!("Rendering texture");
    queue.submit(Some(encoder.finish()));

    let buffer_slice = output_buffer.slice(..);
    let buffer_future = buffer_slice.map_async(wgpu::MapMode::Read);
    device.poll(wgpu::Maintain::Wait);
    if let Ok(()) = buffer_future.await {
        let t2 = std::time::Instant::now().duration_since(t1);
        dbg!("Finished", t2);
        let data = buffer_slice.get_mapped_range();
        let result: Vec<f32> = data
            .chunks_exact(4)
            .map(|b| f32::from_ne_bytes(b.try_into().unwrap()))
            //.filter(|&x| x > 0.8)
            .collect();
        for i in (0..6) {
            let bytes_per_face = 16*256*256;
            dbg!(result.len());
            let bmp_data: Vec<[u8; 3]> = result[(256*256*i)*4..(256*256*(i+1)*4)].chunks(4).map(|x| {
                let r = x[0];
                let g = x[1];
                let b = x[2];
                let a = x[3];

                [(a / 1.0 * 255.0) as u8,
                (b / 1.0 * 255.0) as u8,
                (g / 1.0 * 255.0) as u8]
            }).collect();
            bmp::write_image(bytemuck::cast_slice(&bmp_data), 256, 256, 8*3, &format!("file{}.bmp", i));
            
        }
    }
}
