# cubemap-compute
This repo is for testing new features that maybe someday will make their way into my game engine / game `wgpu-test` + **my own .bmp writer**.

I used python for debugging / verifying that my shader math was correct. (see `test_shader_math.ipynb`)

Currently in the works:
 * Convert equirectangular HDR environment map images [(examples)](https://polyhaven.com/hdris) to cubemap textures using the GPU.
 * Calculate mipmaps using gpu.
 * Calculate textures to be used for image based diffuse / specular lighting
 * Test and benchmark fragment shader vs compute shader

 Issues:
  * Very bright small objects cause sampling artefects in the diffuse irradiance texture
  * Adding more samples causes weird issues. (Shader timeout?)
  * Adding wgpu `QuerySets` for benchmarking causes weird async issues. (Driver or error in wgpu vulkan implementation?) Maybe open a new issue on wgpu-rs github.
