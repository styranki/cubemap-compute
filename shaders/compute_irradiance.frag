#version 450
layout(location=0) out vec4 FragColor;
layout(location=0) in vec3 WorldPos;

layout(set = 1, binding = 0) uniform textureCube t_environment;
layout(set = 1, binding = 1) uniform sampler s_environment;

const float PI = 3.14159265359;

void main() {
    vec3 N = normalize(WorldPos);

    vec3 irradiance = vec3(0.0);

    vec3 up = vec3(0.0, 1.0, 0.0);
    vec3 right = normalize(cross(up, N));
    up = normalize(cross(N, right));

    float sampleDelta = 0.005;
    float nrSamples = 0.0;
    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta) {
        for(float theta= 0.0; theta < 0.5 * PI; theta += sampleDelta) {
            vec3 tangentSample = vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));

            vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;
            irradiance += textureLod(samplerCube(t_environment, s_environment), sampleVec, 1.0).xyz * cos(theta) * sin(theta);
            nrSamples += 1;
        }
    }

    irradiance = PI * irradiance * (1.0 / float(nrSamples));

    FragColor = vec4(irradiance, 1.0);

}
