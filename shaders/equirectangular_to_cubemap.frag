#version 450

layout(location=0) out vec4 FragColor;
layout(location=0) in vec3 WorldPos;

layout(set = 1, binding = 0) uniform texture2D t_environment;
layout(set = 1, binding = 1) uniform sampler s_environment;

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

void main() {
    vec2 uv = SampleSphericalMap(normalize(WorldPos));
    vec4 color = texture(sampler2D(t_environment, s_environment), uv);

    //FragColor = vec4(color, 1.0);
    FragColor = color;
}
