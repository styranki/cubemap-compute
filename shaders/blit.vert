#version 450
layout (location = 0) in vec2 xy;

layout (location = 0) out vec2 tex_coords;

void main() {
    float x = xy.x;
    float y = xy.y;

    tex_coords = vec2(1.0 * x, 1.0 * y);
    gl_Position = vec4( tex_coords.x * 2.0 - 1.0,
                     1.0 - tex_coords.y * 2.0,
                     0.0, 1.0);
}
