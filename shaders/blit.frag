#version 450

layout (location = 0) in vec2 tex_coords;


layout(location=0) out vec4 FragColor;

layout (set = 0, binding = 0) uniform texture2D r_color;
layout (set = 0, binding = 1) uniform sampler r_sampler;

void main() {
    FragColor = texture(sampler2D(r_color, r_sampler), tex_coords);
}
