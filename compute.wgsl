[[binding(0), group(0)]]
var src_tex: texture_2d<f32>;
[[binding(1), group(0)]]
var src_sampler: sampler;
[[binding(2), group(0)]]
var dst_tex: texture_storage_2d_array<rgba32float, write>;
[[binding(3), group(0)]]
var dst_sampler: sampler;

fn cubeCoordToWorld(cubeCoord: vec2<f32>, face_idx: u32) -> vec3<f32> {
	switch(face_idx) {
		case 0: {return vec3<f32>(1.0, 1.0 * cubeCoord.y, cubeCoord.x);} // posx
		case 1: {return vec3<f32>(-1.0, 1.0 * cubeCoord.y, -1.0 * cubeCoord.x);} //negx
		case 2: {return vec3<f32>(cubeCoord.y, 1.0, -1.0*cubeCoord.x);} // posy
		case 3: {return vec3<f32>(-1.0 * cubeCoord.y, -1.0, -1.0 * cubeCoord.x);} // negy
		case 4: {return vec3<f32>(-1.0 * cubeCoord.x, cubeCoord.y, 1.0);} //posz
		case 5: {return vec3<f32>(cubeCoord.x, 1.0 * cubeCoord.y, -1.0);} //negz
        default: {return vec3<f32>(0.0);} // unreachable
  	}
  	return vec3<f32>(0.0);
}

//fn cartesion_to_spherical(cart: vec3<f32>) -> vec2<f32> {
//	let r = sqrt(cart.x * cart.x + cart.y * cart.y + cart.z * cart.z);
//	let latitude = atan2(carx ,y) 
//}

struct CubemapCoord {
	uv: vec2<f32>;
	face_id: u32;
};

fn spherical_to_cubemap_uv(spherical_coords: vec2<f32>) -> CubemapCoord {
	let theta = spherical_coords.x;
	let phi = spherical_coords.y;
	let pi = 3.1415;

	var cartesian = vec3<f32>(
		sin(phi) * sin(theta) * 1.0,
		cos(theta),
		cos(phi) * sin(theta) * 1.0
	);

	var greatest_index = 0;
	if (cartesian.y >= cartesian.x && cartesian.y >= cartesian.z) {
		greatest_index = 1;
	}
	if (cartesian.z >= cartesian.x && cartesian.z >= cartesian.y) {
		greatest_index = 2;
	}

	cartesian = cartesian / max(max(abs(cartesian.x), abs(cartesian.y)), abs(cartesian.z));
	let cart = cartesian;
	var cube: CubemapCoord = CubemapCoord(vec2<f32>(0.0, 0.0), u32(0));

	switch(greatest_index) {
	 	case 0: {
	 		if(cart.x > 0.0) {
	 			cube.face_id = u32(0);
	 			cube.uv = vec2<f32>(cart.z, cart.y);
	 		} else {
				cube.face_id = u32(1);
				cube.uv = vec2<f32>(-cart.z, cart.y);
	 		}
	    }
		case 1: {
			if(cart.y > 0.0) {
				cube.face_id = u32(2);
				cube.uv = vec2<f32>(-cart.x, cart.z);
			} else {
				cube.face_id = u32(3);
				cube.uv = vec2<f32>(-cart.x, -cart.z);
			}
		}
		case 2: {
			if(cart.z > 0.0) {
				cube.face_id = u32(4);
				cube.uv = vec2<f32>(-cart.x, cart.y);
			} else {
				cube.face_id = u32(5);
				cube.uv = vec2<f32>(cart.x, cart.y);
			}
		}
        default: {
        }
	}

	return cube;
}

fn cubemap_to_spherical(uv: vec2<f32>, face_id: u32) -> vec2<f32> {
	let cart = normalize(cubeCoordToWorld(uv, face_id));
    //cart = cart / sqrt(cart.x * cart.x + cart.y * cart.y + cart.z * cart.z);
	let theta = acos(cart.y);
	let phi = atan2(cart.z, cart.x) % (2.0 * 3.14159);
	return vec2<f32>(phi, theta);
}

[[stage(compute), workgroup_size(1)]]
fn main([[builtin(global_invocation_id)]] param: vec3<u32>) {
	let uv = vec2<f32>(f32(param.x) / 256.0 * 2.0 - 1.0, f32(param.y) / 256.0 * 2.0 - 1.0);
	let spherical_coords = cubemap_to_spherical(uv, param.z);
	let  uv_src: vec2<f32> = vec2<f32>(spherical_coords.x / (2.0 * 3.14159), (spherical_coords.y) / (3.14159) );
	//var color: vec4<f32> = textureLoad(src_tex, coords, 0);
	var color: vec4<f32> = textureSampleLevel(src_tex, src_sampler, uv_src, 0.0);
	//var color: vec4<f32> = vec4<f32>(cubeCoordToWorld(cubeCoords, param.z), 0.0);

	textureStore(dst_tex,
	vec2<i32>(i32(param.x), i32(param.y)),
	i32(param.z),
	color,
	);
	return;
 }
